﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {
    [SerializeField]
    protected GameObject EnemyPrefab;
    [SerializeField]
    protected int enemySpawnValue = 1;
    protected List<GameObject> Enemy_Pool = new List<GameObject>();

    void OnEnable()
    {
       
        if (Enemy_Pool.Count > 0)
        {
            foreach (GameObject item in Enemy_Pool)
            {
                    Destroy(item);
            }
            Enemy_Pool.Clear();
        }
        if (Enemy_Pool.Count == 0)
        {
            for (int i = 0; i < enemySpawnValue; i++)
            {
                GameObject enemyObject = Instantiate(EnemyPrefab) as GameObject;
                enemyObject.transform.name = "ENEMY " + i;
                enemyObject.transform.position = new Vector3(transform.position.x +i*2, enemyObject.transform.position.y, enemyObject.transform.position.z);  
                enemyObject.transform.SetParent(transform, true);
                Enemy_Pool.Add(enemyObject);
            }
        }
    }

}
