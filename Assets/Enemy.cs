﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.InfiniteRunnerEngine
{

    public class Enemy : MonoBehaviour
    {
        protected void OnTriggerEnter2D(Collider2D other)
        {
            TriggerEnter(other.gameObject);
        }

        protected void OnTriggerEnter(Collider other)
        {
            TriggerEnter(other.gameObject);
        }

        protected void TriggerEnter(GameObject collidingObject)
        {
         
            if (collidingObject.tag != "Player") { return; }

            PlayableCharacter player = collidingObject.GetComponent<PlayableCharacter>();
            if (player==null) { return; }
            if (LevelManager.Instance.characterAttackPermit)
            {
                LevelManager.Instance.KillCharacter(player);
            }
            else
            {
                GameManager.Instance.AddPoints(1000);
                Destroy(gameObject);
            }
        }
    }
}
