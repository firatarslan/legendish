﻿/* 
 * 
 * LEGENDISH CHARACTER CONTROLLER 
 *
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.InfiniteRunnerEngine;
using Lean.Touch;

public class LegendishCharacter : Jumper
{
    private LeanFingerSwipe swipeRight;
    

    [Header("BASH")]
    [SerializeField]
    private float downForceBash = 5; 
    [SerializeField]
    private float bashAreaDamage = 20;
    [SerializeField]
    private GameObject bashEffect=null;

    protected bool bashEffectEnable = false;
    
    [Header("DASH")]
    [SerializeField]
    private float dashSpeedCoefficient = 10;
    [SerializeField]
    private float dashEndingDuration = 5;
    [SerializeField]
    private float minSwipeMagnitude = 5;

    protected bool dashAttackPermit = false;

    [Header("SFX")]
    [SerializeField]
    private AudioClip jumpSFX=null;
    [SerializeField]
    private AudioClip characterDeathSFX = null;
    [SerializeField]
    private AudioClip bashSFX = null;
    public AudioClip enemyDeathSFX = null;
    
    protected override void Initialize()
    {
        base.Initialize();
    }
    protected override void Start()
    {
        base.Start();
        swipeRight = FindObjectOfType<LeanFingerSwipe>();
        swipeRight.OnSwipeDelta.AddListener(delegate(Vector2 delta) {
            if (delta.x > delta.y && delta.magnitude > minSwipeMagnitude && _grounded)
                Dash();
        });
        
       LeanTouch.OnFingerTap += OnFingerTap;      
    }

    void OnFingerTap(LeanFinger finger)
    {
        Jump();  
    }

    void OnDisable()
    {
        SoundManager.Instance.PlaySound(characterDeathSFX, transform.position);
        LeanTouch.OnFingerTap -= OnFingerTap;
    }

    protected override void Update()
    {
            base.Update();

            if (dashAttackPermit)
            {
                Collider[] dashColliders = Physics.OverlapBox(transform.position, transform.localScale);
                hitCollider(dashColliders);
            }
            if (_grounded)
            {
                if (bashEffectEnable)
                {
                    if (bashEffect != null)
                    {
                        Debug.Log("Bash - ");
                        Collider[] bashColliders = Physics.OverlapSphere(transform.position, bashAreaDamage);
                        hitCollider(bashColliders);
                        GameObject explosionBash = Instantiate(bashEffect);
                        explosionBash.transform.position = transform.position;
                        SoundManager.Instance.PlaySound(bashSFX,transform.position);
                        bashEffectEnable = false;
                        LevelManager.Instance.characterAttackPermit = true;
                    }
                }
            }
    }
    public override void Jump()
    {
        if (EvaluateJumpConditions())
        {
            base.Jump();
            if(jumpSFX != null)
            SoundManager.Instance.PlaySound(jumpSFX, transform.position);
        }
        else
            Bash();
    }

    protected override bool EvaluateJumpConditions()
    {
        return _grounded;
    }

    public void Dash()
    {
        DashSpeedUp();
        Invoke("DashSpeedDown", dashEndingDuration);
    }

    public void Bash()
    {
        bashEffectEnable = true;
        LevelManager.Instance.characterAttackPermit = false;
        _rigidbodyInterface.AddForce(Vector3.down * JumpForce * downForceBash);
    }

    void DashSpeedUp()
    {
        Debug.Log("Dash - speed up");
        dashAttackPermit = true;
        LevelManager.Instance.characterAttackPermit = false;
        LevelManager.Instance.MaximumSpeed *= dashSpeedCoefficient;
        LevelManager.Instance.SetSpeed(LevelManager.Instance.Speed * dashSpeedCoefficient);
    }

    void DashSpeedDown()
    {
        Debug.Log("Dash - speed down");
        LevelManager.Instance.SetSpeed(LevelManager.Instance.Speed / dashSpeedCoefficient);
        LevelManager.Instance.MaximumSpeed /= dashSpeedCoefficient;
        dashAttackPermit = false;
        LevelManager.Instance.characterAttackPermit = true;
    }

    protected void hitCollider(Collider[] _collider) {
        for (int i = 0; i < _collider.Length; i++)
        {
            if (_collider[i].tag == "Enemy")
            {
                if (enemyDeathSFX != null)
                {
                    SoundManager.Instance.PlaySound(enemyDeathSFX, transform.position);
                    GameManager.Instance.AddPoints(1000);
                    Destroy(_collider[i].gameObject);
                }
            }
        }
    }
}
